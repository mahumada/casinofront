import React, { Component } from "react";
import Slider from "react-slick";

const GameCarousel = ({ games }) => {
    const settings = {
      dots: false,
      infinite: false,
      speed: 500,
      slidesToShow: 4,
      slidesToScroll: 4,
      initialSlide: 0,
    };
    return (
        <Slider {...settings} style={{width: '60vw', height: '12vw', background: 'grey', paddingLeft: '2.5vw'}}>
          {games.map((game)=>{
            return (
              <div>
                <a href={game.url} style={{display: "flex", flexDirection: 'column', alignItems: 'center', background: 'white', overflow: 'hidden', height: '12vw', width: '12vw', borderRadius: '0vw', color: 'black', textDecoration: 'none'}}>
                  <h4 style={{fontSize: '1vw', margin: '1vh 0 1vh 0'}}>{game.title}</h4>
                  <img src={'assets/games/' + game.url + '.png'} style={{width: '10vw', height: '14vh', borderRadius: '1vw'}}/>
                  <p style={{margin: '1vh 0 0 0', textAlign: 'center', fontSize: '.8vw', paddingLeft: '.5vw', paddingRight: '.5vw'}}>{game.description}</p>
                </a>
              </div>
            )
          })}
        </Slider>
    );
}

export default GameCarousel;