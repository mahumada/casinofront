import React, { useState, useEffect } from 'react';
import { NavLink, Navbar, Links, Logo, NavBtn, LinksPopup } from './styles.js';
import { FaBars } from 'react-icons/fa';
import store from '../../../store/reducers/store.js';
import storeUserInit from '../../../functions/storeUserInit';

function NavBar() {

  const [username, setUsername] = useState(store.getState().username);
  
  useEffect(async()=>{
    await storeUserInit();
    console.log('done')
  }, []);

  store.subscribe(()=>{
    if(username !== store.getState().username){
      setUsername(store.getState().username);
    }
  })

  const [toggleNavLinks, setToggleNavLinks] = useState(false);
  const [dimensions, setDimensions] = React.useState({ 
    height: window.innerHeight,
    width: window.innerWidth
  })
  React.useEffect(() => {
    function handleResize() {
      setDimensions({
        height: window.innerHeight,
        width: window.innerWidth
      })
}
    window.addEventListener('resize', handleResize)
    return _ => {
      window.removeEventListener('resize', handleResize)
}
  })

  return (
    <>
      <Navbar>
        <Logo src="/assets/logo.png"/>
        {dimensions.width > 650 ? (
        <Links>
          <NavLink href="/">HOME</NavLink>
          <NavLink href="/blackjack">BLACKJACK</NavLink>
          <NavLink href="/roulette">ROULETTE</NavLink>
          <NavLink href="/poker">POKER</NavLink>
          <NavLink href={username ? '/profile' : '/login'} style={{color: username ? 'gold' : 'white'}}>{username ? username.toUpperCase() : 'LOGIN'}</NavLink>
        </Links>
        ) : (
          <NavBtn onClick={() => setToggleNavLinks(!toggleNavLinks)}>
            <FaBars/>
          </NavBtn>
        )}
      </Navbar>
        {toggleNavLinks && dimensions.width < 650 ? (
          <LinksPopup>
            <NavLink href="/">HOME</NavLink>
            <NavLink href="/">HOME</NavLink>
            <NavLink href="/">HOME</NavLink>
            <NavLink href="/">HOME</NavLink>
            <NavLink href="/">HOME</NavLink>
          </LinksPopup>
        ) : ''}
    </>
  )
}

export default NavBar;