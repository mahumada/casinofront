import styled from 'styled-components';
import * as colors from '../../../utils/colors/index.js';

export const Navbar = styled.nav`
  background: black;
  color: white;
  width: 100vw;
  height: 15vh;
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: space-between;
  border-bottom: 1vh gold solid;
  border-bottom-left-radius: 3vh;
  overflow: hidden;
`

export const Logo = styled.img`
  height: 15vh;
  width: 15vh;
  background: gold;
`

export const Name = styled.h1`
  font-size: 3vmax;
  color: white;
`

export const Links = styled.div`
  width: 50vw;
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: space-evenly;
`

export const NavLink = styled.a`
  color: ${colors.light};
  text-decoration: none;
  font-size: 3.5vmin;
  transition: 1s;

  &:hover {
    font-size: 2.2vmax;
    color: gold;
  }
`

export const NavBtn = styled.button`
  height: 10vh;
  width: 10vh;
  margin-right: 5vw;
  background: ${colors.dark};
  border: .5vw solid ${colors.light};
  border-radius: 25%;
  transition: 1s;
  color: ${colors.light};
  font-size: 13vw;
  display: flex;
  justify-content: center;
  align-items: center;

  &:hover {
    color: ${colors.dark};
    background: ${colors.light};
    border-color: ${colors.dark};
  }
`

export const LinksPopup = styled.div`
  position: fixed;
  background: ${colors.light};
  display: flex;
  width: 100vw;
  flex-direction: column;
  align-items: flex-end;
  justify-content: space-evenly;

  & ${NavLink} {
    color: ${colors.dark};
    margin: 1.5vh;
    font-size: 4vmax;
  }
  `