import styled from 'styled-components';

export const Bar = styled.div`
  height: 12%;
  width: 100%;
  position: absolute;
  bottom: 0;
  left: 0;
  z-index: 9999999;
  // background: white;
  border-top-left-radius: 10%;
  border-top-right-radius: 10%;
  opacity: .9;
  color: black;
  display: flex;
  align-items: center;
  justify-content: center;
  font-size: 2.25rem;
`

export const ExitBtn = styled.button`
  position: absolute;
  top: 10%;
  right: 1%;
  background: black;
  color: white;
  font-size: 2rem;
  border: none;
  width: 2.5rem;
  height: 2.5rem;
  border-radius: 1rem;
  transition: 1s;
  &:hover {
    transform: scale(1.1);
  }
`