import styled from 'styled-components';

export const Btn = styled.button`
  font-size: 2.5vmax;
  color: black;
  background: gold;
  border: .2vw solid black;
  border-radius: 1vw;
  transition: 1s;
  margin-bottom: .1%;
  position: relative;
  padding: .4rem 1rem .4rem 1rem;
  margin-right: 4%;
  @media (max-width: 650px){
    display: none;
  }

  @media not handheld {
    &:hover {
      color: gold;
      background: black;
      border: .15vw solid gold;
      border-radius: 1vw;
      transform: scale(1.1);
      box-shadow: 1vh 1vh 2vh black;
    }
  }

`