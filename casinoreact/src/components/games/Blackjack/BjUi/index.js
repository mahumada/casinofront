import React from 'react';
import { Wrapper, UiChip, UiChipRenderer } from './styles.js';
import addBet from '../../../../store/actionCreators/addBet.js';
import store from '../../../../store/reducers/store.js';
import setBetChips from '../../../../store/actionCreators/setBetChips.js';
import setGames from '../../../../store/actionCreators/setGames.js';
import {Howl, Howler} from 'howler';

function BjUi({index}) {
  let bet = store.getState().BJchips;

  store.subscribe(()=>{
    bet = store.getState().BJchips;
    Howler.volume(store.getState().volume);
  })

  const addChip = (prop) => {
    bet[index][prop] += 1;
    let newGames = store.getState().games;
    newGames[index] = true;
    setGames(newGames)
    setBetChips(bet);
  }

  let sound = new Howl({
    src: ['assets/sound/chip.mp3']
  })

  const chipList = [
    [1, 'one'],
    [5, 'five'],
    [10, 'ten'],
    [25, 'twentyfive'],
    [50, 'fifty'],
    [100, 'hundred'],
    [200, 'twohundred'],
    [500, 'fivehundred'],
    [1000, 'thousand']
  ]

  const playChip = (num, str) => {
    sound.play();
    addBet(index, num);
    addChip(str);
  }

  return (
    <Wrapper>
      {chipList.map((chip, index)=>{
        const str = `assets/fichas/f${chip[0]}.png`;
        console.log(str);
        return (
          <UiChip key={index}>
            <UiChipRenderer src={str} onClick={() => playChip(...chip)}/>
          </UiChip>
        )
      })}
    </Wrapper>
  )
}

export default BjUi;
