import styled from 'styled-components';

export const Alert = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  position: absolute;
  padding: 1vw;
  font-size: 3vw;
  border-radius: 2vw;
  height: 10%;
  z-index: 999999;
`