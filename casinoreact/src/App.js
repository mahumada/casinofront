import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import { Home, Blackjack, Roulette, Error, Login, Signup, Profile } from './layout/index.js';

function App() {
  
  return (
    <Router>
      <Switch>
        <Route exact path="/">
          <Home />
        </Route>
        <Route path="/blackjack">
          <Blackjack />
        </Route>
        <Route path="/roulette">
          <Roulette />
        </Route>
        <Route path="/login">
          <Login />
        </Route>
        <Route path="/signup">
          <Signup />
        </Route>
        <Route path="/profile">
          <Profile />
        </Route>
        <Route path="*">
          <Error />
        </Route>
      </Switch>
    </Router>
  );
}

export default App;
