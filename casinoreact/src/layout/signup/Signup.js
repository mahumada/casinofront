import React, { useState } from 'react';
import { useForm } from 'react-hook-form';
import { Form, TextInput, BtnSubmit } from './styles';
import Navbar from '../../components/navigation/NavBar';
import * as api from '../../controllers';

const Login = () => {

  const [style, setStyle] = useState({});
  const { register, handleSubmit, formState: { errors }} = useForm();

  const onSubmit = async(data) => {
    const response = await api.createUser(data);
    if(response['error']){
      setStyle({ background: 'red' });
    }else{
      window.location = '/';
    }
  }
  
  return (
    <>
      <Navbar />
      <Form onSubmit={handleSubmit(onSubmit)}>
        <TextInput type="text" placeholder="username" {...register("username")}/>
        <TextInput type="password" placeholder="password" {...register("password")}/>
        <TextInput type="firstName" placeholder="first name" {...register("firstName")}/>
        <TextInput type="lastName" placeholder="lastName" {...register("lastName")}/>
        <TextInput type="email" placeholder="email" {...register("email")}/>
        <TextInput type="phone" placeholder="phone" {...register("phone")}/>
        <BtnSubmit type="submit" style={style}>SUBMIT</BtnSubmit>
      </Form>
    </>
  )
}

export default Login;