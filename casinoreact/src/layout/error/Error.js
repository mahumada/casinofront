import React from 'react';
import { Link } from 'react-router-dom';

function Error() {
  return (
    <div style={{display: 'flex', flexDirection: 'column', alignItems: 'center', justifyContent: 'center', height: '100vh'}}>
      <h1 style={{fontSize: '4vmax', color: 'gold'}}>ERROR 404: Page not found</h1>
      <Link to="/" style={{color: 'gold', fontSize: '3vmax'}}>HOME</Link>
    </div>
  )
}

export default Error;