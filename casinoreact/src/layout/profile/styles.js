import styled from "styled-components"

export const Wrapper = styled.div`
  width: 100vw;
  height: 100vh;
  display: flex;
  align-items: center;
  justify-content: center;
`

export const Container = styled.section`
  width: 55vw;
  height: 80vh;
  background: grey;
  color: black;
  padding: 2vw;
  display: flex;
  align-items: center;
  justify-content: space-evenly;
  border-radius: 4vw;
  flex-direction: column;
`

export const Info = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-evenly;
  width: 85%;
`

export const Coins = styled.div`
  width: 90%;
  display: flex;
  flex-direction: column;
  height: 25%;
  flex-wrap: wrap;
  align-items: center;
`

export const ButtonContainer = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-evenly;
  width: 60%;
`

export const BtnSubmit = styled.button`
  font-size: 2.25vw;
  background: gold;
  border: 1vh solid black;
  border-radius: 1.5vw;
  padding: 1.5vh;
  transition: .6s;
  margin: 0 1vw 0 1vw;
  &:hover {
    transform: scale(1.05);
  }
`