import React, { useState, useEffect } from 'react';
import { BtnSubmit, Container, Wrapper, ButtonContainer, Info, Coins } from './styles';
import * as api from '../../controllers';
import delete_cookie from '../../functions/deleteCookie';

function Profile() {
 
  const [user, setUser] = useState(undefined);
  const [coins, setCoins] = useState('');

  // User data loading
  useEffect(async() => {
    const result = await api.getUserData();
    const {coins, ...user} = result;
    setUser(user);
    console.log(coins)
    let newCoins = [];
    if(coins){
      for(let coin in coins){
        newCoins.push([]);
        newCoins[newCoins.length-1][0] = coin;
        newCoins[newCoins.length-1][1] = coins[coin];
      }
    }
    setCoins(newCoins);
  }, [])

  // Redirect if not logged in
  if(user && user['error']){
    window.location = '/login';
  }

  return (
    <Wrapper>
      {user ? (
        <Container>
          <Info>
            <img src="https://www.personality-insights.com/wp-content/uploads/2017/12/default-profile-pic-e1513291410505.jpg" style={{width: '10vw', height: '10vw', borderRadius: '50%'}}/>
            <div>
              <h1 style={{fontSize: '3vw'}}>{user.firstName} {user.lastName}</h1>
              <h3>{user.email}</h3>
            </div>
          </Info>
          <Coins>
            {coins && coins.length ? coins.map((coin)=>{
              return <h3 style={{color: 'gold'}}>{coin[0].toUpperCase()} - {coin[1]}</h3>
            }) : <h1>NO COINS</h1>}
          </Coins>
          <ButtonContainer>
            <BtnSubmit type="button" onClick={async() => {console.log(await api.logout()); delete_cookie('connect.sid'); window.location = '/'}} style={{background: 'red'}}>LOGOUT</BtnSubmit>
            <BtnSubmit type="button" onClick={async() => {window.location = '/'}} style={{background: 'white'}}>HOME</BtnSubmit>
          </ButtonContainer>
        </Container>
      ) : <h1 style={{color: 'gold', fontSize: '3vw'}}>LOADING...</h1>}
    </Wrapper>
  )
}

export default Profile;