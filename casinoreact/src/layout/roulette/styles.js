import styled from 'styled-components';
import * as colors from "../../utils/colors";
import * as measures from "../../utils/measures";
export const World = styled.div`
  @import url('https://fonts.googleapis.com/css2?family=Nunito+Sans:wght@800&family=Open+Sans:wght@400;500;700&display=swap');
  width: 80%;
  height: 100vh;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  margin: 0 auto;
  *{
      font-family: 'Open Sans', sans-serif;
  }
  
`
export const Container = styled.div`
  width: 100%;
  height: 30vw;
  display: flex;
  align-items:center;
  position: relative;
  margin-left: -10vw;
  margin-top: -8vw;
`


export const BetTable = styled.div`
  background: #17680A;
  width: 100%;
  height: 100%;
  border: ${measures.thickness} solid #fff;
  display: flex;
  flex-direction: column;
  & > div:not(:last-child){
      width: 100%;
  flex: 1 0 auto;
  display: flex;
  & > div:not(:last-child){
     
  flex: 1 0 auto;
  border-bottom: ${measures.thickness} solid #fff;
  border-right: ${measures.thickness} solid #fff;
  }
  & > div:last-child{
      
  flex: 1 0 auto;
  border-bottom: ${measures.thickness} solid #fff;
  }   
  }

   & > div:first-child{
       width: 100%;
  flex: 1 0 auto;
  display: flex;
  
  & > div:not(:last-child){
    
  flex: 1 0 auto;
  &:nth-child(1){
      background: red;
  }
  &:nth-child(2){
      background: black;
  }
  &:nth-child(3){
      background: red;
  }
   &:nth-child(4){
      background: red;
  }
   &:nth-child(5){
      background: black;
  }
   &:nth-child(6){
      background: red;
  }
   &:nth-child(7){
      background: red;
  }
   &:nth-child(8){
       background: black;
  }
   &:nth-child(9){
      background: red;
  }
   &:nth-child(10){
      background: red;
  }
   &:nth-child(11){
      background: black;
  }
   &:nth-child(12){
      background: red;
  }
 

  
  border-right: ${measures.thickness} solid #fff;
  }
  & > div:last-child{
   
      flex: 1 0 auto;
      & > div{
          transform: translateY(-2vw);
          width: 10px;
      }
  }

  }


   & > div:nth-child(2){
       width: 100%;
  flex: 1 0 auto;
  display: flex;
  
  & > div:not(:last-child){
    
  flex: 1 0 auto;
  &:nth-child(1){
      background: black;
  }
  &:nth-child(2){
      background: red;
  }
  &:nth-child(3){
      background: black;
  }
   &:nth-child(4){
      background: black;
  }
   &:nth-child(5){
      background: red;
  }
   &:nth-child(6){
      background: black;
  }
   &:nth-child(7){
      background: black;
  }
   &:nth-child(8){
      background: red;
  }
   &:nth-child(9){
      background: black;
  }
   &:nth-child(10){
      background: black;
  }
   &:nth-child(11){
      background: red;
  }
   &:nth-child(12){
      background: black;
  }
 

  
  border-right: ${measures.thickness} solid #fff;
  }
  & > div:last-child{
   
      flex: 1 0 auto;
      & > div{
          transform: translateY(-2vw);
          width: 10px;
      }
     
  }

  }
  
      
  
  & > div:last-child{
       width: 100%;
  flex: 1 0 auto;
  display: flex;
  
  & > div:not(:last-child){
    
  flex: 1 0 auto;
  &:nth-child(1){
      background: red;
  }
  &:nth-child(2){
      background: black;
  }
  &:nth-child(3){
      background: red;
  }
   &:nth-child(4){
      background: black;
  }
   &:nth-child(5){
      background: black;
  }
   &:nth-child(6){
      background: red;
  }
   &:nth-child(7){
      background: red;
  }
   &:nth-child(8){
      background: black;
  }
   &:nth-child(9){
      background: red;
  }
   &:nth-child(10){
      background: black;
  }
   &:nth-child(11){
      background: black;
  }
   &:nth-child(12){
      background: red;
  }
 

  
  border-right: ${measures.thickness} solid #fff;
  }
  & > div:last-child{
   
      flex: 1 0 auto;
      & > div{
          transform: translateY(-2vw);
          width: 10px;
      }
  }

  }

`
export const FootBetTable = styled.div`
  position: absolute;
  top: 30.7vw; 
  right: 5.5vw;
  background: ${colors.tableColor};
  border-bottom: ${measures.thickness} solid #fff;
  border-left: ${measures.thickness} solid #fff;
  border-right: ${measures.thickness} solid #fff;
  
  width: 74%;
  height: 10vw;
  display: flex;
  flex-direction: column;
  & > div:first-child{
      flex: 1 0 auto;
      border-bottom: ${measures.thickness} solid white;
      display: flex;
      & > div:not(:last-child){
          border-right: ${measures.thickness} solid white;
          flex: 1 0 auto;
          color: #fff;
          font-size: 3vw;
          position: relative;
          &:after{
              content: "1st12";
              position: absolute;
              top: 0;
              left: 30%;
          }
          &:nth-child(2):after{
              content: "2nd12";
          }
      }
      & > div:last-child{
          flex: 1 0 auto;
          position: relative;
           color: #fff;
          font-size: 3vw;
           &:after{
              content: "3rd12";
              position: absolute;
              top: 0;
              left: 30%;
          }
      }
  }
  & > div:last-child{
      flex: 1 0 auto;
      display: flex;
      & > div:first-child{
          border-right: ${measures.thickness} solid white;
          flex: 1 0 auto;
          display: flex;
          & > div{
              flex: 1 0 auto;
              position: relative;
             
              &:first-child:after{
                  content: "1to18";
                  position: absolute;
                  top: 1vw;
                  left: 1.8vw;
                  font-size: 2vw;
                  color: #fff;
              }
               &:last-child:after{
                  content: "PAR";
                  position: absolute;
                  top: 1vw;
                  left: 1.8vw;
                  font-size: 2vw;
                  color: #fff;
              }
              
          }
          & > div:first-child{
              border-right: ${measures.thickness} solid white;
          }
      }
      & > div:nth-child(2){
          border-right: ${measures.thickness} solid white;
          flex: 1 0 auto;
          display: flex;
          & > div{
              flex: 1 0 auto;
              position: relative;
              & > img{
                  position: absolute;
                  top: 0;
                  left: 0.3vw;
                  width: 9vw;
                  height: 4.5vw;

              }
            
              
          }
          & > div:first-child{
              border-right: ${measures.thickness} solid white;
          }
      }
      & > div:last-child{
          flex: 1 0 auto;
          display: flex;
         
       
          & > div{
              flex: 1 0 auto;
              position: relative;
               &:first-child:after{
                  content: "IMPAR";
                  position: absolute;
                  top: 1vw;
                  left: 1.8vw;
                  font-size: 2vw;
                  color: #fff;
              }
               &:last-child:after{
                  content: "19to36";
                  position: absolute;
                  top: 1vw;
                  left: 1.8vw;
                  font-size: 2vw;
                  color: #fff;
              }
              
          }
          & > div:first-child{
              border-right: ${measures.thickness} solid white;
          }
          
      }
      
  }
`


export const Row = styled.div`
  
`
export const Cell = styled.div`
position: relative;
  & > div{
     position: absolute;
     top: 3vw;
     left: 1vw;
     font-size: 2vw;
     color: #fff;
  }
`



export const SideTriangle = styled.div`
  width: 17vw;
  height: 100%;
  display: flex;
  align-items: center;
  & > div:first-child{
    
      height: 100%;
       flex: 5 0 auto;
       position: relative;
       &:after{
           content: "";
           position: absolute;
           right: 0;
           top: 0;
           border-top: 15vw solid black; 
            border-left: 4vw solid black; 
             border-bottom: 15vw solid black; 
              border-right: 4vw solid ${colors.tableColor}; 
            
       }
       & > div{
           position: absolute;
           top: 13.5vw;
           left: 9vw;
           z-index: 99;
           font-size: 2vw;
           color: #fff;
       }
       
  }
  & > div:last-child{
      background: ${colors.tableColor};
      height: 100%;
      flex: 1 0 auto;
      border-top: ${measures.thickness} solid #fff;
      border-bottom: ${measures.thickness} solid #fff;
  }
`


export const Coins = styled.div`
transform: translate(0vw,12vw);

  img{
      width: 5vw;
      height: 5vw;
      
  }
`