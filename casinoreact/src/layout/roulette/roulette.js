import React, { useState, useEffect } from 'react';
import { World, Container, BetTable, Row, Cell, SideTriangle, FootBetTable, Coins } from "./styles.js";

function Roulette() { 
  const onBet = () => {
    console.log("bet");
  }
  return (
    <>
     <World>
       <Container>
         <SideTriangle>
           <div><div>0</div></div>
           <div></div>
            </SideTriangle>
            <BetTable>
              
                <Row>
                <Cell  onClick={onBet}><div>3</div></Cell>
                <Cell onClick={onBet}><div>6</div></Cell>
                <Cell onClick={onBet}><div>9</div></Cell>
                <Cell onClick={onBet}><div>12</div></Cell>
                <Cell onClick={onBet}><div>15</div></Cell>
                <Cell onClick={onBet}><div>18</div></Cell>
                <Cell onClick={onBet}><div>21</div></Cell>
                <Cell onClick={onBet}><div>24</div></Cell>
                <Cell onClick={onBet}><div>27</div></Cell>
                <Cell onClick={onBet}><div>30</div></Cell>
                <Cell onClick={onBet}><div>33</div></Cell>
                <Cell onClick={onBet}><div>36</div></Cell>
                <Cell onClick={onBet}><div><div>
                    2
                  </div>
                  <div>to</div>
                  <div>1</div>
                  </div></Cell>
              </Row>
              <Row>
                <Cell onClick={onBet}><div>2</div></Cell>
                <Cell onClick={onBet}><div>5</div></Cell>
                <Cell onClick={onBet}><div>8</div></Cell>
                <Cell onClick={onBet}><div>11</div></Cell>
                <Cell onClick={onBet}><div>14</div></Cell>
                <Cell onClick={onBet}><div>17</div></Cell>
                <Cell onClick={onBet}><div>20</div></Cell>
                <Cell onClick={onBet}><div>23</div></Cell>
                <Cell onClick={onBet}><div>26</div></Cell>
                <Cell onClick={onBet}><div>29</div></Cell>
                <Cell onClick={onBet}><div>32</div></Cell>
                <Cell onClick={onBet}><div>35</div></Cell>
                <Cell><div><div>
                    2
                  </div>
                  <div>to</div>
                  <div>1</div>
                  </div></Cell>
              </Row>
              <Row>
                <Cell onClick={onBet}><div>1</div></Cell>
                <Cell onClick={onBet}><div>4</div></Cell>
                <Cell onClick={onBet}><div>7</div></Cell>
                <Cell onClick={onBet}><div>10</div></Cell>
                <Cell onClick={onBet}><div>13</div></Cell>
                <Cell onClick={onBet}><div>16</div></Cell>
                <Cell onClick={onBet}><div>19</div></Cell>
                <Cell onClick={onBet}><div>22</div></Cell>
                <Cell onClick={onBet}><div>25</div></Cell>
                <Cell onClick={onBet}><div>28</div></Cell>
                <Cell onClick={onBet}><div>31</div></Cell>
                <Cell onClick={onBet}><div>34</div></Cell>
                <Cell onClick={onBet}><div><div>
                    2
                  </div>
                  <div>to</div>
                  <div>1</div>
                  </div></Cell>
              </Row>
              
            </BetTable>
            <FootBetTable>
                  <div>
                    <div onClick={onBet}></div>
                    <div onClick={onBet}></div>
                    <div onClick={onBet}></div>
                  </div>
                  <div>
                    <div>
                      <div onClick={onBet}></div>
                      <div onClick={onBet}></div>
                    </div>
                    <div>
                      <div onClick={onBet}><img src="http://localhost:3000/assets/roulette/Red.png" alt="" /></div>
                      <div onClick={onBet}><img src="http://localhost:3000/assets/roulette/Black.png" alt="" /></div>
                    </div>
                    <div>
                      <div onClick={onBet}></div>
                      <div onClick={onBet}></div>
                    </div>
                  </div>
                </FootBetTable>
            
            
         </Container>
         <Coins>
           <img src="http://localhost:3000/assets/fichas/f5.png" alt="" />
                      <img src="http://localhost:3000/assets/fichas/f25.png" alt="" />
           <img src="http://localhost:3000/assets/fichas/f100.png" alt="" />
           <img src="http://localhost:3000/assets/fichas/f500.png" alt="" />
           <img src="http://localhost:3000/assets/fichas/f1000.png" alt="" />

         </Coins>
         
      </World>
    </>
  )
}

export default Roulette;