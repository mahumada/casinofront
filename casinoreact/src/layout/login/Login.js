import React, { useState } from 'react';
import { useForm } from 'react-hook-form';
import { Form, TextInput, BtnSubmit } from './styles';
import Navbar from '../../components/navigation/NavBar';
import * as api from '../../controllers';
import delete_cookie from '../../functions/deleteCookie';
import setUsername from '../../store/actionCreators/setUsername';
import store from '../../store/reducers/store';

const Login = () => {

  const [style, setStyle] = useState({});
  const { register, handleSubmit, formState: { errors }} = useForm();

  const onSubmit = async(data) => {
    const response = await api.login(data);
    if(response['error']){
      setStyle({ background: 'red' });
    }else{
      window.location = '/';
    }
  }
  
  return (
    <>
      <Navbar />
      <Form onSubmit={handleSubmit(onSubmit)}>
        <TextInput type="text" placeholder="username" {...register("username")}/>
        <TextInput type="password" placeholder="password" {...register("password")}/>
        <BtnSubmit type="submit" style={style}>SUBMIT</BtnSubmit>
        <BtnSubmit type="button" style={{transform: 'scale(.85)'}} onClick={() => window.location = '/signup'}>SIGNUP</BtnSubmit>
      </Form>
    </>
  )
}

export default Login;