import styled from 'styled-components';

export const Form = styled.form`
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: space-evenly;
  height: 60vh;
  margin-top: 10vh;
  background: gold;
  width: 40vw;
  margin-left: 30vw;
  border-radius: 5vw;
`


export const TextInput = styled.input`
  background: black;
  border: none;
  font-size: 2vw;
  color: white;
  text-align: center;
  padding: 1vh;
  border-radius: 2vw;
  transition: 1s;

  &:hover {
    transform: scale(1.05);
  }
`

export const BtnSubmit = styled.button`
  font-size: 2.25vw;
  background: gold;
  border: 1vh solid black;
  border-radius: 1.5vw;
  padding: 1.5vh;
  transition: .6s;
  &:hover {
    background: black;
    color: gold;
  }
`