import styled from 'styled-components';

export const Title = styled.h1`
  text-align: center;
  font-size: 4vw;
  color: gold;
`

export const Games = styled.section`
  width: 100vw;
  height: 85vh;
  margin-top: 15vh;
  display: flex;
  flex-direction: column;
  align-items: center;
`

export const GameWrapper = styled.div`
  display: flex;
  flex-direction: row;
  flex-wrap: wrap;
  align-items: center;
  justify-content: space-evenly;
`

export const Game = styled.a`
  width: 20vw;
  height: 20vw;
  margin-top: 1%;
  background: gold;
  text-decoration: none;
  color: black;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  border-radius: 3vw;
  text-align: center;
  overflow: hidden;
  transition: 1s;
  &:hover {
    transform: scale(1.07)
  }
`

export const Banners = styled.section`
  marginTop: 15vh;
  width: 100vw;
  height: 100vh;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  marginLeft: 20vw;
`