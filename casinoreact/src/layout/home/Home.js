import React, { useState, useEffect } from 'react';
import NavBar from '../../components/navigation/NavBar';
import { Carousel } from 'react-responsive-carousel';
import "react-responsive-carousel/lib/styles/carousel.min.css";
import { Games, Title, Banners, GameWrapper, Game } from './styles';
import GameCarousel from '../../components/navigation/GameCarousel';
import store from '../../store/reducers/store';


const Home = () => {

  const gameList = [
    {title: 'Blackjack', url: 'Blackjack', description: 'Play the famous card game right now!'},
    {title: 'Roulette', url: 'Roulette', description: 'Play the famous roulette game in our platform!'},
    {title: 'Poker', url: 'Poker', description: 'Online poker to play with friends and strangers2!'},
    {title: 'Blackjack', url: 'Blackjack', description: 'Play the famous card game right now!'},
    {title: 'Roulette', url: 'Roulette', description: 'Play the famous roulette game in our platform!'},
    {title: 'Poker', url: 'Poker', description: 'Online poker to play with friends and strangers2!'},
    {title: 'Blackjack', url: 'Blackjack', description: 'Play the famous card game right now!'},
    {title: 'Roulette', url: 'Roulette', description: 'Play the famous roulette game in our platform!'},
    {title: 'Poker', url: 'Poker', description: 'Online poker to play with friends and strangers2!'},
  ]

  return (
    <>
      <NavBar />
      <Games id="games">
        <Title>GAMES</Title>
        <GameCarousel games={gameList}/>
      </Games>
      <Banners id="banners">
        <Title>LATEST</Title>
        <Carousel autoPlay={true} interval={5000} infiniteLoop={true} width='60vw'>
          <div>
              <img src="assets/banner-1.png" />
              <p className="legend">Lorem Ipsum 1</p>
          </div>
          <div>
              <img src="assets/banner-2.png" />
              <p className="legend">Lorem ipsum 2</p>
          </div>
        </Carousel>
      </Banners>
    </>
  )
}

export default Home;
